import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class QRPrinterMain {
	
	static boolean devMode = false;
	static boolean debugMode = false;
	
	static URL splashLogoPath = QRPrinterMain.class.getResource("/SplashLogo.png");
	static URL iconPath = QRPrinterMain.class.getResource("/QR Icon.png");

	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); // Set Look and Feel
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Encountered error while loading application. Error Code: 70\n" + e.toString());
			e.printStackTrace();
			System.exit(0);
		} catch (InstantiationException e) {
			JOptionPane.showMessageDialog(null, "Encountered error while loading application. Error Code: 71\n" + e.toString());
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			JOptionPane.showMessageDialog(null, "Encountered error while loading application. Error Code: 72\n" + e.toString());
			e.printStackTrace();
			System.exit(0);
		} catch (UnsupportedLookAndFeelException e) {
			JOptionPane.showMessageDialog(null, "Encountered error while loading application. Error Code: 73\n" + e.toString());
			e.printStackTrace();
			System.exit(0);
		}
		
		// Splash Screen	
		if(!devMode){
			JWindow window = new JWindow();
			
			window.add(new JLabel(new ImageIcon(splashLogoPath)));
			
			window.setBounds(0, 0, 889, 500);
			
			window.setBackground(new Color(255,255,255,0));
			window.setVisible(true);
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize(); // Used to center the JFrame on the screen.
			window.setLocationRelativeTo(null);
			window.setLocation(dim.width/2-window.getSize().width/2, dim.height/2-window.getSize().height/2); // Set the starting location of the JFrame to the center of the screen
	
			try {
			    Thread.sleep(3000);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			}
			window.setVisible(false);
			window.dispose();
		}
		// Splash Screen End
		
		//Start the menu
		@SuppressWarnings("unused")
		PrintScreen pr = new PrintScreen();
		
	}

}
