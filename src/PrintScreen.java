import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PrintScreen extends JFrame {
	
	Dimension centerDim = Toolkit.getDefaultToolkit().getScreenSize(); // Used to center the JFrame on the screen.	
	
	JLabel titleLbl = new JLabel("HC Maintenance - QR Printer");
	JLabel dataLbl = new JLabel("QR Data");
	JTextField dataTextField = new JTextField();
	JButton printBtn = new JButton("Print");
	
	String printData;	
	int row = 0;
	
	public PrintScreen(){
		
		super("HC Maintenance - QR Printer");
		
		setIconImage(new ImageIcon(QRPrinterMain.iconPath).getImage());
		
		// Specify an action for the close button.
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create a layout manager for the content pane.
		setLayout(new GridBagLayout());
		
		// Create constraints for the screen components and add them.
		// note: constraints can be changed independently for each component.
		GridBagConstraints c = new GridBagConstraints();
		
		c.anchor = GridBagConstraints.PAGE_END;
		c.insets = new Insets(10,10,10,10); //(top,left,bottom,right)
		
		//titleLbl
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = .5;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = row;
		add(titleLbl, c);
		
		//QRDataLbl
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = .5;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = ++row;
		add(dataLbl, c);
		
		//QRDataTextField
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = .5;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = ++row;
		dataTextField.addActionListener(new PrintBtnListener());
		add(dataTextField, c);
		
		//PrintButton
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = .5;
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = ++row;
		printBtn.addActionListener(new PrintBtnListener());
		add(printBtn, c);
		
		pack();
		setLocationRelativeTo(null);
		setSize(new Dimension(400,200));
		setLocation(centerDim.width/2-this.getSize().width/2, centerDim.height/2-this.getSize().height/2); // Set the starting location of the JFrame to the center of the screen
		setVisible(true);
		
	}
	
	private class PrintBtnListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			try{
				
				boolean twoLines = false;
				int QRMagnification = 8;
				int textWidth = 30;
				int textHeight = 50;
				String printString = null;
				printData = dataTextField.getText().trim();
				
				//Resize data to match 1.5x1.5 label stock
				if(printData.length() > 30){
					JOptionPane.showMessageDialog(null, "60 Characters is the maximum character limit.\nYour data is at: " + printData.length() + " characters long");
					return;
				}
				
				if(printData.length() > 20){
					textWidth = 18;
				}
				
				if(printData.length() > 30){
					textWidth = 18;
					textHeight = 25;
					twoLines = true;
				}
				
				if(printData.length() > 38){
					QRMagnification = 7;
				}
								
				Socket clientSocket=new Socket("10.23.45.17", 9100); 
				DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream() );
				
		        printString = "^XA"
		        	+ "^FO20,50^A0N," + textHeight + "," + textWidth +"^FD" + printData + "^FS"
					+ "^FO20,100"
					+ "^BQ,2," + QRMagnification
					+ "^FDA," + printData + "^FS" //space after the comma is intentional and necessary.
					+ "^XZ";		
		        
		        if(twoLines){
		        	printString = "^XA"
			        	+ "^FO20,50^A0N," + textHeight + "," + textWidth +"^FD" + printData.substring(0, 30) + "^FS"
			        	+ "^FO20,76^A0N," + textHeight + "," + textWidth +"^FD" + printData.substring(30) + "^FS"
						+ "^FO20,100"
						+ "^BQ,2," + QRMagnification
						+ "^FDA," + printData + "^FS" //space after the comma is intentional and necessary.
						+ "^XZ";
		        }
		        		        
		        if(QRPrinterMain.debugMode)
		        	System.out.println("Printing: " + printString);
	
				outToServer.writeBytes(printString);
		        
				clientSocket.close();
			} catch (IOException er) {
				JOptionPane.showMessageDialog(null, "Encountered issues while trying to print. Error code: 21\n" + er.toString());
			}
			
		}
		
	}

}
